class AddContribExtensions < ActiveRecord::Migration
  def change
    enable_extension 'pg_trgm'
    enable_extension 'fuzzystrmatch'
    enable_extension 'unaccent'
  end
end
