class UserPolicy < ApplicationPolicy

  def initialize(person, user)
    @user   = person
    @record = user
  end

  def index?
    true
  end

  def show?
    return false unless user.present?
    super
  end

  def create?
    admin?
  end

  def update?
    admin?
  end

  def destroy?
    admin?
  end

  def search?
    admin?
  end

  private

  def admin?
    user.present? and user.has_role? :admin
  end
end
