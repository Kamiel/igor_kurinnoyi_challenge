class User < ActiveRecord::Base
  include PgSearch

  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_attached_file :avatar,
    styles: { small: '20x20', med: '40x40', large: '160x160' }

  validates_attachment_content_type :avatar,
    content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  # FIXME: https://github.com/Casecommons/pg_search/issues/303
  pg_search_scope :search_by_login,
                  against: [:name, :email],
                  using: { trigram:    {},
                           dmetaphone: {},
                           tsearch: { prefix: true,
                                        highlight: {
                                          start_sel: '<mark>',
                                          stop_sel: '</mark>'
                                        }
                                     }
                         },
                  ignoring: [:accents]

  def login
    name.present? ? name : email
  end

end
