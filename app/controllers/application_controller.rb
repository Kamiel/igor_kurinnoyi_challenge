require "application_responder"

class ApplicationController < ActionController::Base
  self.responder = ApplicationResponder
  respond_to :html

  include Pundit
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit :sign_up,
      keys: [:name, :avatar, { role_ids: [] }]
    devise_parameter_sanitizer.permit :account_update,
      keys: [:name, :avatar, { role_ids: [] }]
  end

  private

  def user_not_authorized
    redirect_to(request.referrer || root_path,
      flash: {alert: "You are not authorized to perform this action."})
  end

end
