class UsersController < ApplicationController
  before_action :verify_request_format!
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :authorize_user

  utf8_enforcer_workaround

  responders :flash, :http_cache, Responders::PaginateResponder
  respond_to :html, :json, :js
  # responders and 'respond_wiht' some why not working that way
  # TODO: debug it.

  USERS_MAX_ON_PAGE = 3

  # GET /users
  # GET /users.json
  def index
    # @users = User.page(params[:page]).per USERS_MAX_ON_PAGE
    # respond_with @users
    respond_to do |format|
      format.html do
        @users = User.page(params[:page]).per USERS_MAX_ON_PAGE
      end
      format.json do
        @users = User.all
      end
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    respond_to :html, :json
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
    respond_to :html
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user,
                      notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user,
                      notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url,
                    notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /users/search
  # GET /users/search.json
  def search
    result = User.search_by_login params[:query]

    if result
      respond_to do |format|
        format.html do
          @users = result.with_pg_search_highlight
            .page(params[:page]).per USERS_MAX_ON_PAGE
          render :index
        end
        format.js # TODO
        format.json do
          @users = result
          render :index
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to :index }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  # setup paginate responder
  def per_page
    USERS_MAX_ON_PAGE
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  def authorize_user
    authorize(@user || User)
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params_without_password.require(:user).permit :email, :password,
    :password_confirmation, :name, :avatar, { role_ids: [] }
    #                unsafe here a little --------------^
  end

  def params_without_password
    if params[:user][:password].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end
    params
  end
end
