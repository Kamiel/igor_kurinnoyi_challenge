source 'https://rubygems.org'
ruby '2.3.0'


gem 'puma'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.5'
# Use sqlite3 as the database for Active Record
# gem 'sqlite3'
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

gem 'responders'

gem 'font-awesome-sass'
gem 'bootstrap'
gem 'rails-assets-tether'
gem 'autoprefixer-rails'
gem 'bootstrap_flash_messages'

gem 'simple_form'

gem 'devise'
gem 'pundit'
gem 'rolify'
# to set up admin role by default
gem 'migration_data'

gem 'paperclip'

gem 'kaminari'
gem 'paginate-responder'

# Markup

gem 'slim-rails'
# Add logic-free view mustache support to slim templates
# gem 'slim-mustache'
gem 'html2slim', require: false

# Search

gem 'pg_search', github: 'Casecommons/pg_search',
                 ref: 'e4e2ed236690e27337a8ed0d31d38d7721f35a5c'
gem 'utf8_enforcer_workaround'

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  gem 'pry', require: false
  gem 'pry-rails'
  gem 'pry-remote', require: false
  gem 'byebug'
  gem 'pry-byebug'
  gem 'pry-stack_explorer', require: false
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

group :test do
  gem 'rspec-rails'
  gem 'factory_girl_rails'
  gem 'spring-commands-rspec'
  gem 'guard-rspec'
end
